Brainrust
=========
This is a simple experiment I did to learn some Rust. Nothing useful 
about it.

It runs a Brainfuck program in interpreter mode, though it includes some 
trivial optimizations.

If you want to try it anyway, create a program file named 
"program.bf", install rust and cargo, and then type "cargo run".
