use bf::instruction;
use bf::environment;

/// Optimizes various consecutive ChangeMemoryValue instructions
/// into a single ChangeMemoryValue instruction
fn optimize_consecutive_cmv(block : &mut instruction::InstructionBlock) {
	let mut start_cmv_idx : usize = 0;
	let mut consecutive_cmv_count : usize = 0;
	let mut total_cmv : i8 = 0;

	let mut current_idx = 0;
	while current_idx < block.instructions.len() {
		if let Some(v) = (&block.instructions[current_idx]).as_cmv() {
			if consecutive_cmv_count == 0 {
				start_cmv_idx = current_idx;
				consecutive_cmv_count = 1;
				total_cmv = 0;
			} else {
				consecutive_cmv_count += 1;
			}

			total_cmv = total_cmv.wrapping_add(v.value);
		}

		if current_idx+1 == block.instructions.len() ||
			(&block.instructions[current_idx+1]).as_cmv().is_none() {
			if consecutive_cmv_count > 1 {
				for _ in 0 .. consecutive_cmv_count {
					let _ = block.instructions.remove(start_cmv_idx);
				}

				block.instructions.insert(start_cmv_idx,
					Box::new(instruction::ChangeMemoryValue::new(total_cmv)));

				current_idx = start_cmv_idx;
			}

			start_cmv_idx = 0;
			consecutive_cmv_count = 0;
			total_cmv = 0;
		}

		current_idx += 1;
	}
}

/// Optimizes various consecutive ChangeMemoryPointer instructions
/// into a single ChangeMemoryPointer instruction
fn optimize_cmp(block : &mut instruction::InstructionBlock) {
	let mut start_cmp_idx : usize = 0;
	let mut consecutive_cmp_count : usize = 0;
	let mut total_cmp : isize = 0;

	let mut current_idx = 0;
	while current_idx < block.instructions.len() {
		if let Some(v) = (&block.instructions[current_idx]).as_cmp() {
			if consecutive_cmp_count == 0 {
				start_cmp_idx = current_idx;
				consecutive_cmp_count = 1;
				total_cmp = 0;
			} else {
				consecutive_cmp_count += 1;
			}

			total_cmp = total_cmp.wrapping_add(v.value);
		}

		if current_idx+1 == block.instructions.len() ||
			(&block.instructions[current_idx+1]).as_cmp().is_none() {
			if consecutive_cmp_count > 1 {
				for _ in 0 .. consecutive_cmp_count {
					let _ = block.instructions.remove(start_cmp_idx);
				}

				block.instructions.insert(start_cmp_idx,
					Box::new(instruction::ChangeMemoryPointer::new(total_cmp)));

				current_idx = start_cmp_idx;
			}

			start_cmp_idx = 0;
			consecutive_cmp_count = 0;
			total_cmp = 0;
		}

		current_idx += 1;
	}
}

/// Auxiliary instruction for setting the value of the current memory cell to zero
pub struct SetMemoryValueZero {
}

impl SetMemoryValueZero {
	pub fn new() -> SetMemoryValueZero {
		SetMemoryValueZero { }
	}
}

impl instruction::Instruction for SetMemoryValueZero {
	fn unparse(&self) -> String {
		return "[-]".to_string()
	}

	fn run(&self, environment : &mut environment::Environment) {
		environment.memory[environment.memory_pointer] = 0
	}
}

/// Optimizes trivial loops like "[-]",
/// that just set the value of a memory cell to zero
fn optimize_trivial_loops(block : &mut instruction::InstructionBlock) {
	for i in 0 .. block.instructions.len() {
		let mut optimize = false;
		if let Some(l) = (block.instructions[i]).as_loop() {
			if l.block.instructions.len() == 1 {
				if let Some(cmv) = l.block.instructions[0].as_cmv() {
					if cmv.value % 2 != 0 {
						optimize = true;
					}
				}
			}
		}

		if optimize {
			block.instructions[i] = Box::new(SetMemoryValueZero::new())
		}
	}
}

/// Runs all optimizations over the specified instruction block
fn optimize_all(block : &mut instruction::InstructionBlock) {
	optimize_consecutive_cmv(block);
	optimize_cmp(block);
	optimize_trivial_loops(block);
}

/// Optimizes the whole Brainfuck program
pub fn optimize_program(program : &mut instruction::InstructionBlock) {
	instruction::Instruction::walk_blocks(program, optimize_all);
}
