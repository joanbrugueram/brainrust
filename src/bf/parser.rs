use bf::instruction;

/// Parses a instruction block (a whole program or a inner loop)
/// Returns the parsed block, plus the program counter at the end of the block
fn parse_instruction_block(program_chars: &Vec<char>, mut program_counter: usize)
	-> (instruction::InstructionBlock, usize) {
	let mut instructions : Vec<Box<instruction::Instruction>> = Vec::new();

	while program_counter < program_chars.len() {
		match program_chars[{program_counter += 1; program_counter-1}] {
			'>' => {
				instructions.push(Box::new(
					instruction::ChangeMemoryPointer::new(1)));
			}
			'<' => {
				instructions.push(Box::new(
					instruction::ChangeMemoryPointer::new(-1)));
			}
			'+' => {
				instructions.push(Box::new(
					instruction::ChangeMemoryValue::new(1)));
			}
			'-' => {
				instructions.push(Box::new(
					instruction::ChangeMemoryValue::new(-1)));
			}
			'.' => {
				instructions.push(Box::new(
					instruction::PrintCharacter::new()));
			}
			',' => {
				instructions.push(Box::new(
					instruction::GetCharacter::new()));
			}
			'[' => {
				let (subinstructions, new_program_counter) =
					parse_instruction_block(program_chars, program_counter);
				instructions.push(Box::new(
					instruction::Loop::new(subinstructions)));
				program_counter = new_program_counter
			}
			']' => {
				break
			}
			_ => { }
		}
	}

	(instruction::InstructionBlock::new(instructions), program_counter)
}

/// Parse a whole Brainfuck program into an instruction block
pub fn parse_program(program_code: &str) -> instruction::InstructionBlock {
	let program_chars : Vec<char> = program_code.chars().collect();
	let (program, _) = parse_instruction_block(&program_chars, 0);
	program
}
