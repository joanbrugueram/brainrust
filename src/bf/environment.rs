/// Environment to run Brainfuck programs
pub struct Environment {
	pub memory : [u8; 30000],
	pub memory_pointer : usize
}

impl Environment {
	pub fn new() -> Environment {
		Environment {
			memory : [0; 30000],
			memory_pointer : 0
		}
	}
}
