use std;
use std::io::prelude::*;
use bf::environment;

/// Base class used to implement a Brainfuck instruction
pub trait Instruction {
	/// Returns the equivalent Brainfuck code to the instruction
	fn unparse(&self) -> String;

	/// Runs the instruction over the specified environment
	fn run(&self, environment : &mut environment::Environment);

	/// Calls the specified callback function over all instruction blocks
	fn walk_blocks(&mut self, _: fn(&mut InstructionBlock) -> ()) {
	}

	/// Upcast to ChangeMemoryPointer
    fn as_cmp(&self) -> Option<&ChangeMemoryPointer> {
		None
	}

	/// Upcast to ChangeMemoryValue
    fn as_cmv(&self) -> Option<&ChangeMemoryValue> {
		None
	}

	/// Upcast to Loop
    fn as_loop(&self) -> Option<&Loop> {
		None
	}
}

/// Block of Brainfuck instructions
pub struct InstructionBlock {
	pub instructions : Vec<Box<Instruction>> // TODOXXX
}

impl InstructionBlock {
	pub fn new(instructions: Vec<Box<Instruction>>) -> InstructionBlock {
		InstructionBlock {
			instructions : instructions
		}
	}
}

impl Instruction for InstructionBlock {
	fn unparse(&self) -> String {
		let instuctions_unparsed : Vec<String> =
			(&self.instructions).into_iter()
				.map(|s| s.unparse()).collect();
		instuctions_unparsed.join("")
	}

	fn run(&self, environment : &mut environment::Environment) {
		for inst in &self.instructions {
			inst.run(environment)
		}
	}

	fn walk_blocks(&mut self, walker: fn(&mut InstructionBlock) -> ()) {
		for inst in &mut self.instructions {
			inst.walk_blocks(walker)
		}
		walker(self);
	}
}


/// Implementation of the Brainfuck change memory pointer instruction
/// Corresponding instructions: '>', '<'
pub struct ChangeMemoryPointer {
	pub value : isize
}

impl ChangeMemoryPointer {
	pub fn new(value: isize) -> ChangeMemoryPointer {
		ChangeMemoryPointer {
			value: value
		}
	}
}

impl Instruction for ChangeMemoryPointer {
	fn unparse(&self) -> String {
		if self.value >= 0 {
			return String::from_utf8(
				vec![b'>'; self.value as usize]).unwrap()
		} else {
			return String::from_utf8(
				vec![b'<'; -self.value as usize]).unwrap()
		}
	}

	fn run(&self, environment : &mut environment::Environment) {
		environment.memory_pointer =
			environment.memory_pointer.wrapping_add(self.value as usize)
	}

    fn as_cmp(&self) -> Option<&ChangeMemoryPointer> {
		Some(self)
	}
}

/// Implementation of the Brainfuck change memory value instruction
/// Corresponding instructions: '+', '-'
pub struct ChangeMemoryValue {
	pub value : i8
}

impl ChangeMemoryValue {
	pub fn new(value: i8) -> ChangeMemoryValue {
		ChangeMemoryValue {
			value: value
		}
	}
}

impl Instruction for ChangeMemoryValue {
	fn unparse(&self) -> String {
		if self.value >= 0 {
			return String::from_utf8(vec![b'+'; self.value as usize]).unwrap()
		} else {
			return String::from_utf8(vec![b'-'; -self.value as usize]).unwrap()
		}
	}

	fn run(&self, environment : &mut environment::Environment) {
		environment.memory[environment.memory_pointer] =
			environment.memory[environment.memory_pointer].wrapping_add
				(self.value as u8)
	}

    fn as_cmv(&self) -> Option<&ChangeMemoryValue> {
		Some(self)
	}
}


/// Implementation of the Brainfuck print character instruction
/// Corresponding instructions: '.'
pub struct PrintCharacter {
}

impl PrintCharacter {
	pub fn new() -> PrintCharacter {
		PrintCharacter { }
	}
}

impl Instruction for PrintCharacter {
	fn unparse(&self) -> String {
		return ".".to_string()
	}

	fn run(&self, environment : &mut environment::Environment) {
		print!("{}", environment.memory[environment.memory_pointer] as char);
	}
}


/// Implementation of the Brainfuck get character instruction
/// Corresponding instructions: '.'
pub struct GetCharacter {
}

impl GetCharacter {
	pub fn new() -> GetCharacter {
		GetCharacter { }
	}
}

impl Instruction for GetCharacter {
	fn unparse(&self) -> String {
		return ",".to_string()
	}

	fn run(&self, environment : &mut environment::Environment) {
		let mut character = [0; 1];
		std::io::stdin().read_exact(&mut character).unwrap();
		environment.memory[environment.memory_pointer] = character[0];
	}
}

/// Implementation of the Brainfuck loop instruction
/// Corresponding instructions: '[', ']'
pub struct Loop {
	pub block : InstructionBlock
}

impl Loop {
	pub fn new(block: InstructionBlock) -> Loop {
		Loop {
			block : block
		}
	}
}

impl Instruction for Loop {
	fn unparse(&self) -> String {
		return format!("[{}]", self.block.unparse())
	}

	fn run(&self, environment : &mut environment::Environment) {
		while environment.memory[environment.memory_pointer] != 0 {
			self.block.run(environment)
		}
	}

	fn walk_blocks(&mut self, walker: fn(&mut InstructionBlock) -> ()) {
		self.block.walk_blocks(walker)
	}

    fn as_loop(&self) -> Option<&Loop> {
		Some(self)
	}
}
