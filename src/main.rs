use std::io::prelude::*;
use std::fs::File;
use std::io::Error;
mod bf;

fn read_program(program_name : &str) -> Result<String, Error> {
	let mut f = try!(File::open(program_name));
	let mut s = String::new();
	try!(f.read_to_string(&mut s));
	return Ok(s)
}

fn main() {
	let program_code = read_program("program.bf").unwrap();
	let mut program = bf::parser::parse_program(&program_code);
	bf::optimizer::optimize_program(&mut program);
	println!("{}", bf::instruction::Instruction::unparse(&program));

	let mut environment = bf::environment::Environment::new();
	bf::instruction::Instruction::run(&program, &mut environment);
}
